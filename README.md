# 4x4 LGAD-D2-M1 Routing
This is a test project to route a 4x4 LGAD-D2-M1 to a 4x4 ETROC.

The 4x4 LGAD-D2-M1 is an LGAD which follows the size of a standard 4x4 LGAD with the pad size of 1.3 mm,
but the first column of pads is split in two.
In order to keep the number of channels consistent, the last two columns are merged together into one.

## Notes

The current design has only the pad traces, i.e. it is missing the contacts for the guard ring.
Another possible concern is crosstalk, the current design has no shielding and no ground connections, this should keep
the parasitic capacitance of the traces to a minimum.
Adding a pour would help reduce the crosstalk, probably at the cost of increased capacitance.
The same is true for additional layers, connected to ground.

## Previews

### Top

![alt text](img/Top.png "Top view of the 4x4 LGAD-D2-M1 interposer board")

### Bottom

![alt text](img/Bottom.png "Bottom view of the 4x4 LGAD-D2-M1 interposer board")

### Side

![alt text](img/Side.png "Side view of the 4x4 LGAD-D2-M1 interposer board")
